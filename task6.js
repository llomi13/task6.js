//task 1 - Check Winner Team
// საშუალოს დათვლა
function calcAverage(x, y, z) {
  return (x + y + z) / calcAverage.length;
}
// გამარჯვებული გუნდი

function checkWinner() {
  if (average_1 > 2 * average_2) {
    return `powerRangers Team Won ${average_1} vs ${average_2}`;
  } else if (average_2 > 2 * average_1) {
    return `fairyTails Team Won ${average_2} vs ${average_1}`;
  } else {
    return `Neither Of Them Won`;
  }
}

// Implementation Of Functions

let powerRangers = [44, 23, 71];

let average_1 = calcAverage(powerRangers[0], powerRangers[1], powerRangers[2]);

console.log(average_1);

let fairyTails = [65, 54, 49];

let average_2 = calcAverage(fairyTails[0], fairyTails[1], fairyTails[2]);

console.log(average_2);

console.log(checkWinner());

// Task 2 - Tip Calculator

// Problem 1
function calcTip(x) {
  if (x >= 50 && x <= 300) {
    return (y = x * 0.15);
  } else {
    return (y = x * 0.2);
  }
}

// Problem 2
let bills = [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52];

// Problem 3
let tips = [];
for (let i = 0; i < bills.length; i++) {
  tips[i] = calcTip(bills[i]);
}


// Problem 4 

let maxCosts = []

for (let i = 0; i < bills.length; i++) {
    maxCosts[i] = tips[i] + bills[i];
}


//Problem 5

function average (arr) {
    let sum = 0;
    for(let i = 0; i < arr.length; i++){
      sum += arr[i]
    }
    return sum/arr.length
  }
  let totalTips = average(tips);
  let total = average(maxCosts);


  tips.push(totalTips);
  maxCosts.push(total);


  console.log(tips);
  console.log(maxCosts);
  console.log(totalTips, total)